﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Api.Entities
{
    public class User
    {
        [Key]
        [JsonIgnore]
        public int id { get; set; }
        public string user { get; set; }
        [JsonIgnore]
        public byte[] password { get; set; }
        [JsonIgnore]
        public byte[] passwordSalt { get; set; }
        public DateTime fechaCreacion { get; set; }
        
    }
}
