﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Api.Entities
{
    public class Cliente
    {
        [Key]
        [JsonIgnore]
        public int id { get; set; }
        public string primerNombre { get; set; }
        public string? segundoNombre { get; set; }
        public string primerApellido { get; set; }
        public string? segundoApellido { get; set; }
        public string? email { get; set; }
        public string? telefono { get; set; }
        public DateTime? fechaCreacion { get; set; }
        public DateTime? fechaEdicion { get; set; }
    }
}
