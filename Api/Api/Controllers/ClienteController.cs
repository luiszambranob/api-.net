﻿using Api.Context;
using Api.Entities;
using Microsoft.AspNetCore.Mvc;
using Api.Helper;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using Api.Repository;
using System.ComponentModel.DataAnnotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ClienteController : ControllerBase
    {
        private readonly ClienteRepository _clienteRepository;
        public ClienteController( ClienteRepository clienteRepository )
        {
            _clienteRepository = clienteRepository;
        }

        // GET: api/<ValuesController>
        [HttpGet]
        public async Task<ActionResult> Get()
        {

            try
            {
                List<Cliente> data = await _clienteRepository.ListarClientes();

                int codigo = (int)HttpStatusCode.OK;
                string mensaje = "Clientes obtenidos correctamente";
                return RespuestaHelper.RespuestaExitosa(codigo, mensaje, data);

            }
            catch (ValidationException ex)
            {
                int codigo = (int)HttpStatusCode.UnprocessableEntity;
                string mensaje = ex.Message;
                return RespuestaHelper.RespuestaError(codigo, mensaje);
            }
            catch (Exception ex)
            {
                int codigo = (int)HttpStatusCode.BadRequest;
                string mensaje = ex.Message;
                return RespuestaHelper.RespuestaError(codigo, mensaje);
            }

        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {

            try
            {
                Cliente data = await _clienteRepository.ObtenerCliente(id);

                int codigo = (int)HttpStatusCode.OK;
                string mensaje = "Cliente obtenido correctamente";
                return RespuestaHelper.RespuestaExitosa(codigo, mensaje, data);
            }
            catch (ValidationException ex)
            {
                int codigo = (int)HttpStatusCode.UnprocessableEntity;
                string mensaje = ex.Message;
                return RespuestaHelper.RespuestaError(codigo, mensaje);
            }
            catch (Exception ex)
            {
                int codigo = (int)HttpStatusCode.BadRequest;
                string mensaje = ex.Message;
                return RespuestaHelper.RespuestaError(codigo, mensaje);
            }

        }

        // POST api/<ValuesController>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Cliente value)
        {

            try
            {
                Cliente data = await _clienteRepository.CrearCliente(value);

                int codigo = (int)HttpStatusCode.Created;
                string mensaje = "Cliente Creado correctamente";
                return RespuestaHelper.RespuestaExitosa(codigo, mensaje, data);
            }
            catch (ValidationException ex)
            {
                int codigo = (int)HttpStatusCode.UnprocessableEntity;
                string mensaje = ex.Message;
                return RespuestaHelper.RespuestaError(codigo, mensaje);
            }
            catch (Exception ex)
            {
                int codigo = (int)HttpStatusCode.BadRequest;
                string mensaje = ex.Message;
                return RespuestaHelper.RespuestaError(codigo, mensaje);
            }

        }

        // PUT api/<ValuesController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] Cliente value)
        {

            try
            {
                Cliente cliente = await _clienteRepository.EditarCliente(id, value);

                int codigo = (int)HttpStatusCode.OK;
                string mensaje = "Cliente editado correctamente";
                return RespuestaHelper.RespuestaExitosa(codigo, mensaje, cliente);
            }
            catch (ValidationException ex)
            {
                int codigo = (int)HttpStatusCode.UnprocessableEntity;
                string mensaje = ex.Message;
                return RespuestaHelper.RespuestaError(codigo, mensaje);
            }
            catch (Exception ex)
            {
                int codigo = (int)HttpStatusCode.BadRequest;
                string mensaje = ex.Message;
                return RespuestaHelper.RespuestaError(codigo, mensaje);
            }
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _clienteRepository.EliminarCliente(id);

                int codigo = (int)HttpStatusCode.OK;
                string mensaje = "Cliente editado correctamente";
                return RespuestaHelper.RespuestaExitosa(codigo, mensaje, true);
            }
            catch (ValidationException ex)
            {
                int codigo = (int)HttpStatusCode.UnprocessableEntity;
                string mensaje = ex.Message;
                return RespuestaHelper.RespuestaError(codigo, mensaje);
            }
            catch (Exception ex)
            {
                int codigo = (int)HttpStatusCode.BadRequest;
                string mensaje = ex.Message;
                return RespuestaHelper.RespuestaError(codigo, mensaje);
            }
        }
    }
}
