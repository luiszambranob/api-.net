﻿using Api.Context;
using Api.Entities;
using Api.Helper;
using Api.Repository;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Net;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {

        private readonly UserRepository _userRepository;
        public UserController( UserRepository userRepository )
        {
            this._userRepository = userRepository;
        }

        // POST api/<ValuesController>/RegistrarUsuario
        [HttpPost("RegistrarUsuario")]
        public async Task<ActionResult> RegistrarUsuario(UserDto request)
        {
            try
            {
                User user = await _userRepository.RegistrarUsuario(request);

                int codigo = (int) HttpStatusCode.Created;
                string mensaje = "Usuario registrado exitosamente.";

                return RespuestaHelper.RespuestaExitosa( codigo, mensaje, user);
            }catch (ValidationException ex)
            {
                int codigo = (int)HttpStatusCode.UnprocessableEntity;
                string mensaje = ex.Message;

                return RespuestaHelper.RespuestaError(codigo, mensaje);

            }
            catch (Exception ex)
            {
                int codigo = (int)HttpStatusCode.BadRequest;
                string mensaje = ex.Message;

                return RespuestaHelper.RespuestaError(codigo, mensaje);
            }
        }

        // POST api/<ValuesController>/Login
        [HttpPost("Login")]
        public async Task<ActionResult> Login(UserDto request)
        {
            try
            {
                object token = await _userRepository.Login(request);

                int codigo = (int)HttpStatusCode.OK;
                string mensaje = "Acceso exitoso.";

                return RespuestaHelper.RespuestaExitosa(codigo, mensaje, token);
            }
            catch (ValidationException ex)
            {
                int codigo = (int)HttpStatusCode.UnprocessableEntity;
                string mensaje = ex.Message;

                return RespuestaHelper.RespuestaError(codigo, mensaje);

            }
            catch (Exception ex)
            {
                int codigo = (int)HttpStatusCode.BadRequest;
                string mensaje = ex.Message;

                return RespuestaHelper.RespuestaError(codigo, mensaje);
            }
        }
    }
}
