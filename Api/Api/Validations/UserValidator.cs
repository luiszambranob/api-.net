﻿using FluentValidation;
using Api.Entities;
using FluentValidation.Results;

namespace Api.Validations
{
    public class UserValidator : AbstractValidator <UserDto>
    {
        public UserValidator()
        {
            RuleFor(user => user.user).Cascade(CascadeMode.Stop)
                                        .NotEmpty().WithMessage("El campo usuario es requerido")
                                        .Length(4, 10).WithMessage("El campo usuario debe tener minimo 4 caracteres")
                                        .Must(IsValidName).WithMessage("El campo usuario debe contener solo letras.");

            RuleFor(user => user.password).Cascade(CascadeMode.Stop)
                                            .NotEmpty().WithMessage("El campo contrasena es requerido")
                                            .Length(8, 16).WithMessage("El campo contrasena debe tener minimo 8 caracteres");
        }

        private bool IsValidName(string name)
        {
            return name.All(Char.IsLetter);
        }
    }
}
