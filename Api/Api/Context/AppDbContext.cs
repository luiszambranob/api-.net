﻿
using Api.Entities;
using Microsoft.EntityFrameworkCore;

namespace Api.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options):base(options)
        {
        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<User> Users { get; set; }

    }
}
