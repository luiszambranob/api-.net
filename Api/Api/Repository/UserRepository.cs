﻿using Api.Context;
using Api.Entities;
using Api.Validations;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Api.Repository
{
    public class UserRepository
    {
        private readonly AppDbContext   _context;
        private readonly IConfiguration _configuration;
        public UserRepository(AppDbContext context, IConfiguration configuration)
        {
            this._context = context;
            this._configuration = configuration;
        }

        public async Task<User> RegistrarUsuario(UserDto userDto)
        {
            try
            {
                var validator = new UserValidator();
                var result    = validator.Validate(userDto);

                if( !result.IsValid )
                {
                    throw new ValidationException(result.Errors.First().ErrorMessage);
                }

                User? user = await _context.Users.FirstOrDefaultAsync(u => u.user.ToUpper().Equals(userDto.user.ToUpper()));

                if ( user != null )
                {
                    throw new ValidationException("El usuario ya se encuentra registrado.");
                }

                CreateEncryptPassword(userDto.password, out byte[] passworHash, out byte[] passwordSalt);

                user = new User();

                user.user          = userDto.user;
                user.password      = passworHash;
                user.passwordSalt  = passwordSalt;
                user.fechaCreacion = DateTime.Now;

                await _context.Users.AddAsync(user);
                await _context.SaveChangesAsync();

                return user;
            } 
            catch (ValidationException ex)
            {
                throw new ValidationException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al registrar el usuario.");
            }

        }

        public async Task<object> Login(UserDto userDto)
        {
            try
            {
                var validator = new UserValidator();
                var result = validator.Validate(userDto);

                if (!result.IsValid)
                {
                    throw new ValidationException(result.Errors.First().ErrorMessage);
                }

                User? user = await _context.Users.FirstOrDefaultAsync(u => u.user.ToUpper().Equals(userDto.user.ToUpper()));

                if (user == null)
                {
                    throw new ValidationException("No existe una cuenta con ese usuario.");

                }

                if (!VerifyEncryptPassword(userDto.password, user.password, user.passwordSalt))
                {
                    throw new ValidationException("Clave invalida.");

                }

                return CrearToken(user);

            }
            catch (ValidationException ex)
            {
                throw new ValidationException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al iniciar sesion.");
            }

        }

        private static void CreateEncryptPassword(string passwor, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using var hmac = new System.Security.Cryptography.HMACSHA512();

            passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(passwor));
            passwordSalt = hmac.Key;
        }

        private static bool VerifyEncryptPassword(string passwor, byte[] passwordHash, byte[] passwordSalt)
        {
            using var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt);

            var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(passwor));

            return computedHash.SequenceEqual(passwordHash); ;
        }

        private object CrearToken(User user)
        {
            var claim = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.id.ToString()),
                new Claim(ClaimTypes.Name, user.user)
            };

            var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(_configuration.GetSection("appSettings:token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claim),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            var data = new
            {
                user,
                token     = tokenHandler.WriteToken(token),
                expiresAt = DateTime.Now.AddDays(1),
                createAt  = DateTime.Now
            };

            return data;
        }

    }
}
