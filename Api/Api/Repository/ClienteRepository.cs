﻿
using Api.Context;
using Api.Entities;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Web.Http;

namespace Api.Repository
{
    public class ClienteRepository
    {

        private readonly AppDbContext _context;
        private readonly IConfiguration _configuration;
        public ClienteRepository( IConfiguration configuration ,AppDbContext context )
        {
            _configuration = configuration;
            _context       = context;
        }

        public async Task<List<Cliente>> ListarClientes()
        {

            try
            {
                List<Cliente> data = await _context.Clientes.ToListAsync();

                return data;
            }
            catch (ValidationException ex)
            {
                throw new ValidationException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al obtener los clientes.");
            }

        }


        public async Task<Cliente> ObtenerCliente(int id)
        {

            try
            {
                Cliente? data = await _context.Clientes.FindAsync(id);

                if (data == null)
                {
                    throw new ValidationException("No existe el cliente seleccionado.");

                }

                return data;
            }
            catch (ValidationException ex)
            {
                throw new ValidationException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al obtener el cliente.");
            }

        }


        public async Task<Cliente> CrearCliente([FromBody] Cliente value)
        {

            try
            {
                Cliente? cliente = await _context.Clientes.FirstOrDefaultAsync( c => c.primerNombre.ToUpper().Equals(value.primerNombre.ToUpper()) && c.primerApellido.ToUpper().Equals(value.primerApellido.ToUpper()));

                if (cliente != null)
                {
                    throw new ValidationException("El cliente"+value.primerNombre+" "+value.primerApellido+" ya se encuentra registrado.");
                }

                await _context.Clientes.AddAsync(value);
                await _context.SaveChangesAsync();

                return value;
            }
            catch (ValidationException ex)
            {
                throw new ValidationException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al crear el cliente.");
            }
        }

        public async Task<Cliente> EditarCliente(int id, [FromBody] Cliente value)
        {

            try
            {
                Cliente cliente = await _context.Clientes.FindAsync(id);

                cliente.primerNombre    = value.primerNombre;
                cliente.segundoNombre   = value.segundoNombre;
                cliente.primerApellido  = value.primerApellido;
                cliente.segundoApellido = value.segundoApellido;
                cliente.fechaEdicion    = DateTime.Now;

                await _context.SaveChangesAsync();

                return cliente;
            }
            catch (ValidationException ex)
            {
                throw new ValidationException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al editar el cliente.");
            }
        }


        public async Task EliminarCliente(int id)
        {

            try
            {
                Cliente? cliente = await _context.Clientes.FindAsync(id);
                
                if (cliente == null)
                {
                    throw new ValidationException("No existe el cliente seleccionado.");

                }

                _context.Clientes.Remove(cliente);
                await _context.SaveChangesAsync();
            }
            catch (ValidationException ex)
            {
                throw new ValidationException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al eliminar el cliente.");

            }
        }
    }
}