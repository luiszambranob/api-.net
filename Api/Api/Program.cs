
using Api.Config;
using Api.Context;
using Api.Entities;
using Api.Repository;
using Api.Validations;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();

//Configuracion de token
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
       .AddJwtBearer(options =>
       {
           options.TokenValidationParameters = new TokenValidationParameters
           {
               ValidateIssuerSigningKey = true,
               IssuerSigningKey = new SymmetricSecurityKey(
                    System.Text.Encoding.ASCII.GetBytes(builder.Configuration.GetSection("appSettings:token").Value)
               ),
               ValidateIssuer = false,
               ValidateAudience = false,
           };
       });

//Configuracion de repositorios
builder.Services.AddScoped<UserRepository>();
builder.Services.AddScoped<ClienteRepository>();

//Configuracion de las validaciones
builder.Services.AddMvc().AddFluentValidation(config =>{ config.AutomaticValidationEnabled = false; });
builder.Services.AddTransient<IValidator<UserDto>, UserValidator>();

//Es mejor usar la validacion manualmente

//Configuracion de conexion
var connectionString = builder.Configuration.GetConnectionString("sqlServer");
builder.Services.AddDbContext<AppDbContext>(options => options.UseSqlServer(connectionString));

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// Configurar MiddleWare
app.UseMiddleware<ExceptionMiddleware>();
//

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
