﻿using Api.Helper;
using Newtonsoft.Json;
using System.Net;

namespace Api.Config
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _requestDelegate; // delegado del siguiente middleware
        private readonly ILogger _logger;

        public ExceptionMiddleware(RequestDelegate requestDelegate, ILoggerFactory logger)
        {
            _requestDelegate = requestDelegate;
            _logger = logger.CreateLogger<ExceptionMiddleware>();
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _requestDelegate(context);

            }
            catch (WebException ex)
            {
                HttpWebResponse? response = (HttpWebResponse)ex.Response;

                string message = ex.Message;
                int codigo = (int)response.StatusCode;

                await context.Response.WriteAsync( JsonConvert.SerializeObject(new {message, codigo}) );
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                int codigo = 500;

                await context.Response.WriteAsync(JsonConvert.SerializeObject(new { message, codigo }));
            }
        }
    }
}
