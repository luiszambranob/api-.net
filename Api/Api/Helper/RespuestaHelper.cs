﻿using Microsoft.AspNetCore.Mvc;

namespace Api.Helper
{
    public static class RespuestaHelper 
    {

        public static ObjectResult RespuestaExitosa(int codigo, string mensaje, object data) {

            return new ObjectResult(new { codigo ,data, mensaje }) {
                StatusCode = codigo,
            };
        }

        public static ObjectResult RespuestaError(int codigo, string mensaje)
        {

            return new ObjectResult(new { codigo, mensaje })
            {
                StatusCode = codigo,
            };
        }
    }
}
